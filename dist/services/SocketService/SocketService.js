"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const io = require("socket.io");
const DataBase_service_1 = require("../DataBase/DataBase.service");
class SocketService {
    static get Instance() {
        return this._instance || (this._instance = new this());
    }
    constructor() { }
    createSocket(server) {
        const ioC = io(server);
        this._socketConnection = ioC.of('/api/hotel');
        this._socketConnection.on('connection', this._initConnection.bind(this));
    }
    sendHotelProfile(data) {
        this._socketConnection.emit('hotel_send_profile', data);
    }
    sendAllRooms(rooms) {
        this._socketConnection.emit('all_rooms', rooms);
    }
    _handleProfileUpload(data) {
        DataBase_service_1.default.uploadHotelProfile(data);
    }
    _handleHotelRoomUpload(data) {
        DataBase_service_1.default.uploadHotelRoom(data);
    }
    _getHotelProfile(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const hotelProfile = yield DataBase_service_1.default.getHotelProfile(data);
            this._socketConnection.emit('hotel_profile_recieve', hotelProfile);
        });
    }
    _initConnection(socket) {
        socket.on('hotel_profile_load', this._handleProfileUpload);
        socket.on('hotel_room_load', this._handleHotelRoomUpload);
        socket.on('hotel_profile_get', this._getHotelProfile);
    }
    ;
}
exports.default = SocketService.Instance;
//# sourceMappingURL=SocketService.js.map