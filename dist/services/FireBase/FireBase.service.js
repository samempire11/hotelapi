"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const admin = require("firebase-admin");
const firebase_admin_constants_1 = require("../../constants/firebase-admin.constants");
class FireBaseService {
    constructor() {
        this._serviceAccount = firebase_admin_constants_1.fireBaseAdminConfig;
    }
    get App() {
        return this.app;
    }
    get FieldValue() {
        return admin.firestore.FieldValue;
    }
    static get Instance() {
        return this._instance || (this._instance = new this());
    }
    initializeApp() {
        this.app = admin.initializeApp({
            credential: admin.credential.cert(this._serviceAccount),
            databaseURL: "https://test-e2654.firebaseio.com"
        });
    }
    verifyIdToken(req, res, next) {
        next();
        // try {
        //      await this.app.auth().verifyIdToken(token);
        //     return true;
        // } catch(e) {
        //      return false;
        // }
    }
    signUpHotelUser(email, password) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.app.auth().createUser({
                    email: email,
                    emailVerified: false,
                    password: password,
                    disabled: false
                });
                return true;
            }
            catch (e) {
                return false;
            }
        });
    }
    signInHotelUser(token) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.app.auth().verifyIdToken(token);
            }
            catch (e) {
                return e;
            }
        });
    }
}
exports.default = FireBaseService.Instance;
//# sourceMappingURL=FireBase.service.js.map