"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const FireBase_service_1 = require("../FireBase/FireBase.service");
const SocketService_1 = require("../SocketService/SocketService");
class DataBaseService {
    constructor() {
        this.db = FireBase_service_1.default.App.firestore();
        this.FieldValue = FireBase_service_1.default.FieldValue;
        this.Hotel_Profile_Ref = this.db.collection('hotel_profiles');
        this.Live_Auctions_Ref = this.db.collection('live_auctions');
        this.Hotel_Room_Ref = this.db.collection('hotel_rooms');
    }
    static get Instance() {
        return this._instance || (this._instance = new this());
    }
    uploadHotelProfile(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { hotelUserId } = data, rest = __rest(data, ["hotelUserId"]);
            const updateData = rest.name === 'HotelPic' ? {
                [rest.name]: this.FieldValue.arrayUnion(rest.value),
            } : {
                [rest.name]: rest.value,
            };
            yield this.Hotel_Profile_Ref.doc(hotelUserId).update(updateData)
                .then((data) => {
                console.log('suss', data);
            })
                .catch((error) => {
                console.log('errr', error);
                this.Hotel_Profile_Ref.doc(hotelUserId).set(updateData);
            });
        });
    }
    getHotelProfile(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { hotelUserId } = data;
            const hotelProfile = yield this.Hotel_Profile_Ref.doc(hotelUserId).get();
            SocketService_1.default.sendHotelProfile(hotelProfile);
        });
    }
    createHotelRoom(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { hotelUserId, roomId } = data;
            yield this.Hotel_Room_Ref.doc(`${hotelUserId}/${roomId}`).set({});
        });
    }
    getHotelRoom(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { hotelUserId, roomId } = data;
            const hotelRoom = yield this.Hotel_Room_Ref.doc(`${hotelUserId}/${roomId}`).get();
            SocketService_1.default.sendHotelProfile(hotelRoom);
        });
    }
    uploadHotelRoom(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { hotelUserId, roomId, roomData } = data;
            yield this.Hotel_Room_Ref.doc(`${hotelUserId}/${roomId}`).set(roomData);
        });
    }
    createAuction(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const { userId } = data, rest = __rest(data, ["userId"]);
            const currentAuctionByUser = this.Live_Auctions_Ref.doc(userId).get();
        });
    }
}
exports.default = DataBaseService.Instance;
//# sourceMappingURL=DataBase.service.js.map