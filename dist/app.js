"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const helmet = require("helmet");
const cors = require("cors");
const expressValidator = require("express-validator");
const FireBase_service_1 = require("./services/FireBase/FireBase.service");
const allowed_origins_constants_1 = require("./constants/allowed-origins.constants");
const FireBase_service_2 = require("./services/FireBase/FireBase.service");
const Routes_1 = require("./routes/Routes");
const ErrorHandlingService_1 = require("./services/ErrorHandling/ErrorHandlingService");
class App {
    constructor() {
        this._router = new Routes_1.default();
        this.app = express();
        this.config();
        this._router.initRoutes(this.app);
    }
    config() {
        this.app.use(cookieParser());
        this.app.use(express.urlencoded({ extended: true }));
        this.app.use(express.json());
        this.app.use(express.static(path.join(__dirname, 'public')));
        this.app.use(helmet());
        this.app.use(cors({
            origin: function (origin, callback) {
                if (origin === allowed_origins_constants_1.OriginTypes.HOTEL_APP || origin === allowed_origins_constants_1.OriginTypes.HOTEL_WEB) {
                    callback(null, true);
                }
                else {
                    callback(new Error('Not allowed by CORS'));
                }
            },
        }));
        this.app.use(expressValidator());
        this.app.use((req, res, next) => {
            const origin = req.protocol + '://' + req.headers.host;
            if (allowed_origins_constants_1.OriginTypes.HOTEL_WEB === origin && req.originalUrl.search(allowed_origins_constants_1.API_Types.HOTEL) !== -1) {
                res.setHeader('Access-Control-Allow-Origin', origin);
            }
            else if (allowed_origins_constants_1.OriginTypes.HOTEL_APP === origin && req.originalUrl.search(allowed_origins_constants_1.API_Types.USER) !== -1) {
                res.setHeader('Access-Control-Allow-Origin', origin);
            }
            // res.setHeader('Access-Control-Allow-Origin', origin);
            res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
            res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
            res.header('Access-Control-Allow-Credentials', true);
            return next();
        });
        this.app.use(FireBase_service_2.default.verifyIdToken);
        this.app.use(ErrorHandlingService_1.default.errorHandler);
        FireBase_service_1.default.initializeApp();
    }
}
exports.default = new App().app;
//# sourceMappingURL=app.js.map