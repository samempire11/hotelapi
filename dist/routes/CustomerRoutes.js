"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const hotel_routes_constants_1 = require("../constants/hotel-routes.constants");
const validation_rules_constants_1 = require("../constants/validation-rules.constants");
const asyncHandler = require("express-async-handler");
const FireBase_service_1 = require("../services/FireBase/FireBase.service");
class CustomerRoutes {
    initRoutes(app) {
        app.route(hotel_routes_constants_1.HotelRouteConstants.get(hotel_routes_constants_1.MainTypes.sign_up))
            .post(validation_rules_constants_1.validationRules['forRegister'], asyncHandler((req, res) => __awaiter(this, void 0, void 0, function* () {
            //TODO firebase sign up flow
            const errors = validation_rules_constants_1.validationRules.getErrors(req);
            if (typeof errors !== 'boolean') {
                res.status(401).send(JSON.stringify({ data: errors }));
                return;
            }
            const { body: { email, password } } = req;
            const token = yield FireBase_service_1.default.signUpHotelUser(email, password);
            if (!token) {
                res.status(401).send(JSON.stringify({ data: { msg: 'Error while creating User' } }));
                return;
            }
            res.status(200).send(JSON.stringify({ data: { token } }));
        })));
    }
}
exports.default = CustomerRoutes;
//# sourceMappingURL=CustomerRoutes.js.map