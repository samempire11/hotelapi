"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const CustomerRoutes_1 = require("./CustomerRoutes");
const HotelCustomerRoutes_1 = require("./HotelCustomerRoutes");
class Routes {
    constructor() {
        this.customerRoutes = new CustomerRoutes_1.default();
        this.hotelCustomerRoutes = new HotelCustomerRoutes_1.default();
    }
    initRoutes(app) {
        this.customerRoutes.initRoutes(app);
        this.hotelCustomerRoutes.initRoutes(app);
    }
}
exports.default = Routes;
//# sourceMappingURL=Routes.js.map