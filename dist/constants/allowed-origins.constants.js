"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OriginTypes;
(function (OriginTypes) {
    OriginTypes["HOTEL_WEB"] = "http://localhost:3000";
    OriginTypes["HOTEL_APP"] = "http://localhost:4200";
})(OriginTypes = exports.OriginTypes || (exports.OriginTypes = {}));
var API_Types;
(function (API_Types) {
    API_Types["HOTEL"] = "api/hotel";
    API_Types["USER"] = "api/user";
})(API_Types = exports.API_Types || (exports.API_Types = {}));
//# sourceMappingURL=allowed-origins.constants.js.map