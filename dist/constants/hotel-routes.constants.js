"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MainTypes;
(function (MainTypes) {
    MainTypes["sign_up"] = "SignUp";
    MainTypes["sign_in"] = "SignIn";
})(MainTypes = exports.MainTypes || (exports.MainTypes = {}));
exports.HotelRouteConstants = new Map([
    [MainTypes.sign_up, '/api/hotel/register'],
    [MainTypes.sign_in, '/api/hotel/login'],
]);
//# sourceMappingURL=hotel-routes.constants.js.map