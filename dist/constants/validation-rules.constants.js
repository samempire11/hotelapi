"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const check_1 = require("express-validator/check");
exports.validationRules = {
    forRegister: [
        check_1.body('email')
            .isEmail().withMessage('Invalid email format'),
        check_1.body('password')
            .isLength({ min: 6 }).withMessage('Invalid password'),
    ],
    getErrors: (req) => {
        const errors = check_1.validationResult(req);
        return errors.isEmpty() || errors.array();
    },
};
//# sourceMappingURL=validation-rules.constants.js.map