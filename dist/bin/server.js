"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = require("../app");
const http = require("http");
const serverUtils_1 = require("../utils/serverUtils");
const SocketService_1 = require("../services/SocketService/SocketService");
const Server = () => {
    const port = serverUtils_1.normilizePort(process.env.PORT || '3001');
    app_1.default.set('port', port);
    const server = http.createServer(app_1.default);
    SocketService_1.default.createSocket(server);
    server.listen(port);
};
Server();
//# sourceMappingURL=server.js.map