import * as express from 'express';
import * as path from 'path';
import * as cookieParser from 'cookie-parser';
import  * as helmet from 'helmet';
import * as cors from 'cors';
import * as expressValidator from 'express-validator';
import FireBaseAdmin from './services/FireBase/FireBase.service';
import {API_Types, OriginTypes} from "./constants/allowed-origins.constants";
import  FireBaseService from './services/FireBase/FireBase.service';

import Routes from './routes/Routes';
import ErrorHandlingService from "./services/ErrorHandling/ErrorHandlingService";


class App {
   public app: express.Application;
   private _router: Routes = new Routes();

    constructor() {
        this.app = express();
        this.config();
        this._router.initRoutes(this.app);
    }

    private config ():void {
        this.app.use(cookieParser());
        this.app.use(express.urlencoded({ extended: true }));
        this.app.use(express.json());
        this.app.use(express.static(path.join(__dirname, 'public')));
        this.app.use(helmet());
        this.app.use(cors({
            origin: function(origin, callback) {
                console.log('REEE', origin);

                if (origin === OriginTypes.HOTEL_APP || origin === OriginTypes.HOTEL_WEB) {
                callback(null, true);
            } else {
                callback(new Error('Not allowed by CORS'));
            }
            },
        }));
        this.app.use(expressValidator());
        this.app.use((req: any, res: any, next) => {
            const origin = req.protocol + '://' + req.headers.host;
            if (OriginTypes.HOTEL_WEB === origin && req.originalUrl.search(API_Types.HOTEL) !== -1) {
                res.setHeader('Access-Control-Allow-Origin', origin);
            } else if (OriginTypes.HOTEL_APP === origin && req.originalUrl.search(API_Types.USER) !== -1) {
                res.setHeader('Access-Control-Allow-Origin', origin);
            }
            // res.setHeader('Access-Control-Allow-Origin', origin);
            res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
            res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
            res.header('Access-Control-Allow-Credentials', true);
            return next();
        });
        this.app.use(FireBaseService.verifyIdToken);
        this.app.use(ErrorHandlingService.errorHandler);
        FireBaseAdmin.initializeApp();
    }
}

export default new App().app;
