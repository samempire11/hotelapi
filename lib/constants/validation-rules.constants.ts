import {body, validationResult} from "express-validator/check";

export const validationRules = {
    forRegister: [
        body('email')
            .isEmail().withMessage('Invalid email format'),
        body('password')
            .isLength({ min: 6 }).withMessage('Invalid password'),
    ],

    getErrors: (req) => {
        const errors = validationResult(req);
        return errors.isEmpty() || errors.array();

    },
};