export enum MainTypes  {
    sign_up = 'SignUp',
    sign_in = 'SignIn'
}

export const HotelRouteConstants = new Map<any, any> ([
    [MainTypes.sign_up, '/api/hotel/register'],
    [MainTypes.sign_in, '/api/hotel/login'],
]);

export const CustomerRouteConstants = new Map<any, any> ([
    [MainTypes.sign_up, '/api/user/register'],
    [MainTypes.sign_in, '/api/user/login'],
]);
