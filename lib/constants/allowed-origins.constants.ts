export enum OriginTypes {
    HOTEL_WEB = 'http://localhost:3000',
    HOTEL_APP = 'http://localhost:4200'
}

export enum API_Types {
    HOTEL = 'api/hotel',
    USER = 'api/user'
}
