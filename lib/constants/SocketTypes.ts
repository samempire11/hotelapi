export enum SocketTypes {
    profile = 'hotel_profile_load',
    profile_get = 'hotel_profile_get',
    recieve_profile = 'hotel_profile_recieve',
    room = 'hotel_room_load',
    createRoom = 'hotel_room_create',
    removeRoom = 'hotel_room_remove',
    room_get = 'hotel_room_get',
    recieve_room = 'hotel_room_recieve',
    get_all_rooms = 'get_all_rooms',
    recieve_all_rooms = 'recieve_all_rooms',
    setLoggedInHotel = 'set_logged_in_hotel',
}
