import app from '../app';
import * as http from "http";
import * as io from 'socket.io';
import { normilizePort } from "../utils/serverUtils";
import SocketService from "../services/SocketService/SocketService";

const Server = () => {
    const port = normilizePort(process.env.PORT || '3001');
    app.set('port', port);
    const server = http.createServer(app);
    SocketService.createSocket(server);
    server.listen(port);
};

Server();
