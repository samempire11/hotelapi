import {CustomerRouteConstants, MainTypes} from "../constants/hotel-routes.constants";
import {validationRules} from "../constants/validation-rules.constants";
import * as asyncHandler from 'express-async-handler';
import FireBaseService from "../services/FireBase/FireBase.service";

export default class CustomerRoutes {
    public initRoutes(app) {
        app.route(CustomerRouteConstants.get(MainTypes.sign_up))
            .post(validationRules['forRegister'], asyncHandler(async (req, res) => {
                //TODO firebase sign up flow
                console.log('CUSTOMERRRR');
                const errors = validationRules.getErrors(req);
                if ( typeof errors !== 'boolean') {
                    res.status(401).send(JSON.stringify({ data: errors }));
                    return;
                }
                const { body: { email, password } } = req;
                const token = await FireBaseService.signUpHotelUser(email, password, 'user');
                if (!token) {
                    res.status(401).send(JSON.stringify({ data: { msg: 'Error while creating User' } }));
                    return;
                }
                res.status(200).send(JSON.stringify( {data:{ token } }));
            }));
    }
}
