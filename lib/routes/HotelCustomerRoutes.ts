import {HotelRouteConstants, MainTypes} from "../constants/hotel-routes.constants";
import * as asyncHandler from 'express-async-handler';
import  FireBaseService from '../services/FireBase/FireBase.service';
import { validationRules } from '../constants/validation-rules.constants';

export default class HotelCustomerRoutes {
    public initRoutes(app) {

        app.route(HotelRouteConstants.get(MainTypes.sign_up))
            .post(validationRules['forRegister'], asyncHandler(async (req, res) => {
                //TODO firebase sign up flow
                const errors = validationRules.getErrors(req);
               if ( typeof errors !== 'boolean') {
                    res.status(401).send(JSON.stringify({ data: errors }));
                    return;
               }
               const { body: { email, password } } = req;
                const token = await FireBaseService.signUpHotelUser(email, password, 'hotel');
                if (!token) {
                  res.status(401).send(JSON.stringify({ data: { msg: 'Error while creating User' } }));
                  return;
                }
                res.status(200).send(JSON.stringify( {data:{ token } }));
            }));
        app.route(HotelRouteConstants.get(MainTypes.sign_in))
            .post(asyncHandler(async (req, res) => {
                const { headers: { authorization: token } } = req;
                if (!token) {
                    res.end('No Token provided');
                    return;
                }
                const userData = await FireBaseService.signInHotelUser(token);
                if ( userData.code) {
                    res.status(401).send(JSON.stringify({ error: userData.message }));
                    return;
                }
                res.status(200).send(JSON.stringify({ data: { user: userData } }));
            }));
    }
}
