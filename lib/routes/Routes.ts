import CustomerRoutes from './CustomerRoutes';
import HotelCustomerRoutes from './HotelCustomerRoutes';
import {HotelRouteConstants, MainTypes} from "../constants/hotel-routes.constants";

export default class Routes {
    private customerRoutes = new CustomerRoutes();
    private hotelCustomerRoutes = new HotelCustomerRoutes();

    public initRoutes(app) {
    this.customerRoutes.initRoutes(app);
    this.hotelCustomerRoutes.initRoutes(app);
    }
}
