import * as io from 'socket.io';
import * as _ from 'lodash';
import DataBaseService from '../DataBase/DataBase.service';
import {SocketTypes} from "../../constants/SocketTypes";

class SocketService {
    private static _instance: SocketService;
    private _onlineHotelClients = {};
    private _socketConnection: io;

    public static get Instance() {
        return this._instance || (this._instance = new this());
    }

    private constructor() {}

    public createSocket(server) {
        const ioC = io(server);

        this._socketConnection = ioC.of('/api/hotel');
        this._socketConnection.on('connection', this._initConnection.bind(this));
    }

    public sendHotelProfile(data: any): void {

        this._socketConnection.emit('hotel_profile_recieve', data.data());
    }


    public sendAllRooms(rooms: any): void {

        this._socketConnection.emit(SocketTypes.recieve_all_rooms, rooms);
    }

    private _createHotelRoom(data: any) {
        DataBaseService.createHotelRoom(data);
    }

    private _removeHotelRoom(data: any) {
        DataBaseService.removeHotelRoom(data);
    }

    private _getAllRooms(data: any) {
        DataBaseService.getAllHotelRooms(data);
    }

    private _handleProfileUpload(data) {
        DataBaseService.uploadHotelProfile(data);
    }

    private _handleHotelRoomUpload(data) {
        DataBaseService.uploadHotelRoom(data);
    }

    private async _getHotelProfile(data: any) {
        const hotelProfile = await DataBaseService.getHotelProfile(data);
        this._socketConnection.emit('hotel_profile_recieve', hotelProfile);
    }
    private  _setLoggedInHotel(hotelUserId) {
        if (!hotelUserId) {
            return;
        }

        this._onlineHotelClients[hotelUserId] = { hotelUserId };
    }

    private _removedLoggedInHotel (hotelUserId) {
        if (!hotelUserId) {
            return;
        }
        this._onlineHotelClients = _.omit(this._onlineHotelClients, hotelUserId);
    }

    private _initConnection(socket) {
        this._setLoggedInHotel(socket.handshake.query.hotelUserId);
        socket.on(SocketTypes.profile, this._handleProfileUpload.bind(this));
        socket.on(SocketTypes.room, this._handleHotelRoomUpload.bind(this));
        socket.on(SocketTypes.profile_get, this._getHotelProfile.bind(this));
        socket.on(SocketTypes.createRoom, this._createHotelRoom.bind(this));
        socket.on(SocketTypes.get_all_rooms, this._getAllRooms.bind(this));
        socket.on(SocketTypes.removeRoom, this._removeHotelRoom.bind(this));
        socket.on(SocketTypes.setLoggedInHotel, this._setLoggedInHotel.bind(this));
        socket.on('disconnect', this._removedLoggedInHotel.bind(this));
    };

}

export default SocketService.Instance;
