import * as admin from 'firebase-admin';
import { fireBaseAdminConfig } from '../../constants/firebase-admin.constants';

class FireBaseService {

   private static _instance: FireBaseService;
   private  app;
   private _serviceAccount: object = fireBaseAdminConfig;
   private constructor() {}

   get App() {
       return this.app;
   }

   get FieldValue() {
       return admin.firestore.FieldValue;
   }

   public  static get Instance() {
       return this._instance || (this._instance = new this());
   }

   public  initializeApp(): void {
       this.app = admin.initializeApp({
           credential: admin.credential.cert(this._serviceAccount),
           databaseURL: "https://test-e2654.firebaseio.com"
       });
   }

   public  verifyIdToken(req, res, next){
       next();
       // try {
       //      await this.app.auth().verifyIdToken(token);
       //     return true;
       // } catch(e) {
       //      return false;
       // }
   }

   public async signUpHotelUser(email, password, type): Promise<any> {
       try {
           const user = await this.app.auth().createUser({
               email: email,
               emailVerified: false,
               password: password,
               disabled: false,
               displayName: type,
           });
           return true;
       } catch (e) {
        return false;
       }
   }

   public async signInHotelUser(token) {
       try {
           return await this.app.auth().verifyIdToken(token);
       } catch(e) {
            return e;
       }
   }
}

 export default FireBaseService.Instance;
