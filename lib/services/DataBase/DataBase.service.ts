import FireBaseService from '../FireBase/FireBase.service';
import SocketService from "../SocketService/SocketService";
const uniqid = require('uniqid');

class DataBaseService {
    private static _instance: DataBaseService;
    private db;
    private Hotel_Profile_Ref;
    private Hotel_Room_Ref;
    private FieldValue;
    private Live_Auctions_Ref;

    public static get Instance() {
        return this._instance || (this._instance = new this());
    }

    private constructor() {
        this.db = FireBaseService.App.firestore();
        this.FieldValue = FireBaseService.FieldValue;
        this.Hotel_Profile_Ref = this.db.collection('hotel_profiles');
        this.Live_Auctions_Ref = this.db.collection('live_auctions');
        this.Hotel_Room_Ref = this.db.collection('hotel_room_settings');
    }

    private _listenToAuctions() {
        this.Live_Auctions_Ref.where('active', true,).onSnapshot((snap) => {
           snap.docChanges().forEach((change) => {
                switch (change.type) {
                    case 'added':
                    {
                        //TODO fetch all hotels to find right hotels and add auction to it
                    }
                    case 'modified':
                    {
                        //TODO send message to all relevant members of it or update status

                    }
                    case 'removed':
                    {
                        //TODO remove auction from all relevant members of it
                    }
                    default:
                    {

                    }
                }
           });
        });
    }

    public async uploadHotelProfile(data: any): Promise<void> {
        const { hotelUserId, ...rest } = data;
        const updateData = rest.name === 'HotelPic' ? {
            [rest.name]: this.FieldValue.arrayUnion(rest.value),
            } : {
            [rest.name]: rest.value,
        };
            await this.Hotel_Profile_Ref.doc(hotelUserId).update(updateData)
                .then((data) => {
                    console.log('suss', data);
                })
                .catch((error) => {
                    console.log('errr', error);
                    this.Hotel_Profile_Ref.doc(hotelUserId).set(updateData);
                });
    }

    public async getHotelProfile(data: any): Promise<void> {
        const { hotelUserId } = data;
        const hotelProfile = await this.Hotel_Profile_Ref.doc(hotelUserId).get();

        SocketService.sendHotelProfile(hotelProfile);
    }

    public async createHotelRoom(data: any): Promise<void> {
        const { hotelUserId } = data;
        const roomId = uniqid();
        await this.Hotel_Room_Ref.doc(hotelUserId).collection('rooms').doc(roomId).set({
            id: roomId,
        });
        const rooms = await this.Hotel_Room_Ref.doc(hotelUserId).collection('rooms').get();
        SocketService.sendAllRooms(rooms.docs.map(doc => doc.data()));
    }

    public async removeHotelRoom(data: any): Promise<void> {
        const { hotelUserId, roomId } = data;
        await this.Hotel_Room_Ref.doc(hotelUserId).collection('rooms').doc(roomId).delete();
        const rooms = await this.Hotel_Room_Ref.doc(hotelUserId).collection('rooms').get();
        SocketService.sendAllRooms(rooms.docs.map(doc => doc.data()));
    }

    public async getAllHotelRooms(data: any) {
        const { hotelUserId } = data;
        const rooms = await this.Hotel_Room_Ref.doc(hotelUserId).collection('rooms').get();
        SocketService.sendAllRooms(rooms.docs.map(doc => doc.data()));
    }

    public async getHotelRoom(data: any) {
        const { hotelUserId, roomId } = data;
        const hotelRoom = await this.Hotel_Room_Ref.doc(`${hotelUserId}/${roomId}`).get();

        SocketService.sendHotelProfile(hotelRoom);
    }

    public async uploadHotelRoom(data: any): Promise<void> {
        const { hotelUserId, roomId, ...rest } = data;
        const updateData = rest.name === 'RoomPic' ? {
            [rest.name]: this.FieldValue.arrayUnion(rest.value),
        } : {
            [rest.name]: rest.value,
        };
        await this.Hotel_Room_Ref.doc(hotelUserId).collection('rooms').doc(roomId).update(updateData);
    }

    public async createAuction(data: any): Promise<void> {
        const { userId, ...rest } = data;
        const currentAuctionByUser = this.Live_Auctions_Ref.doc(userId).get();
        if (currentAuctionByUser) {
            //TODO send notification aout running auction
            return;
        }


    }

    public async getAllAvailibleAuctions(data: any): Promise<void> {
        const { hotelUserId } = data;
        const hotelProfile = await this.Hotel_Profile_Ref.doc(hotelUserId).get();
        const hotelLatLng = hotelProfile.HotelGeometry.location;
        const allAuctions = [];
        for (let i = 0; i < allAuctions.length; i++) {}
    }
}

export default DataBaseService.Instance;
