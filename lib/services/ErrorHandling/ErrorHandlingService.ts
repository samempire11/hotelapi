class ErrorHandlingService {
    private static _instance: ErrorHandlingService;
    private constructor() {

    }

    public static get Instance() {
        return this._instance || (this._instance = new this());
    }

    public errorHandler(err, req, res, next) {
        console.log("ERRR", err, req.body);
    }
}

export default ErrorHandlingService.Instance;